console.log("Hello World");

// Object Literal
let trainer = {
    name: "Ask Ketchum",
    age: 10,
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
}
console.log(trainer);

console.log("Result of dot Notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of square bracket notation:");
trainer.talk();


// Object Constructor
function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target) {
        console.log(this.name + " tackled " + target.name);
        target.health -= this.attack;
        console.log(`${target.name}'s health is now reduced to ${target.health}`);
        
        if (target.health <= 0) {
            console.log(`${target.name} fainted`);
        }
        console.log(target);
    }

}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);




